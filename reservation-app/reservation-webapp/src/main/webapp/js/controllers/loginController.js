(function() {
	angular.module('reservationApp').controller(
			'LoginController',
			[ '$scope', '$rootScope', '$location', '$cookieStore',
					'UserService', '$translate', '$rootScope', 'UserTokenService', ctrl ]);

	function ctrl($scope, $rootScope, $location, $cookieStore, UserService, $translate, $rootScope, UserTokenService) {
		
	  $scope.changeLanguage = function (langKey) {
		  $translate.use(langKey);
		  $rootScope.selectedLanguage = $translate.use().toUpperCase();
	  };		
		$scope.rememberMe = false;
		$scope.login = function() {
			$rootScope.selectedLanguage = $translate.use().toUpperCase();
			UserService.authenticate($.param({
				username : $scope.username,
				password : $scope.password
			}), function(authenticationResult) {
				var authToken = authenticationResult.token;
				$rootScope.authToken = authToken;
				if ($scope.rememberMe) {
					$cookieStore.put('authToken', authToken);
				}
				$rootScope.user = UserTokenService.get();
				$location.path("/reservations");
			});
		};
	}
})();