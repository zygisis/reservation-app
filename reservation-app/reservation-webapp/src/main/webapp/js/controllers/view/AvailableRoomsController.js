(function() {
	angular.module('reservationApp').controller('AvailableRoomsController', 
			[ '$scope', 'NotifyService', 'RoomService', ctrl ]);

	function ctrl($scope, NotifyService, RoomService) {
		$scope.rooms = RoomService.query();
	}
})();
