(function() {
	angular.module('reservationApp').controller('ReservationController',
			[ '$scope', 'NotifyService', 'ReservationService', ctrl ]);

	function ctrl($scope, NotifyService, ReservationService) {
		$scope.reservations = ReservationService.query();
		$scope.deleteReservation = function(reservation) {
			reservation.$remove(function() {
				$scope.reservations = ReservationService.query();
				NotifyService.showRemovedItem();
			});
		};
	}
})();
