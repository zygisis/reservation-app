(function() {
	angular.module('reservationApp').controller('CreateReservationController',
			[ '$scope', '$location', 'NotifyService', 'ReservationService', 'RoomService', 'UserService', 'ReservationStatusTypesService', '$routeParams', 'RoomService', ctrl ]);

	function ctrl($scope, $location, NotifyService, ReservationService, RoomService, UserService, ReservationStatusTypesService, $routeParams, RoomService) {
		$('#sandbox-container input').datepicker({
			weekStart: 1,
			format: "yyyy-mm-dd",
		    todayBtn: "linked",
		    language: "en",
		    calendarWeeks: true,
		    autoclose: true,
		    todayHighlight: true
		});
		$scope.reservationStatusTypes = ReservationStatusTypesService.query();
		$scope.users = UserService.query();
		$scope.availableRooms = RoomService.query();		
		$scope.reservation = new ReservationService();
		if ($routeParams.roomId != null) {
			$scope.reservation.room = RoomService.get({id: $routeParams.roomId}); 	
		}
		$scope.save = function() {
			$scope.reservation.$save(function() {
				$location.path('/reservations');
				NotifyService.showSavedItem();
			});
		};
	}
})();
