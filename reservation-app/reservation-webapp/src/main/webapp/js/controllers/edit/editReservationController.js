(function() {
	angular.module('reservationApp').controller('EditReservationController',
			[ '$scope', '$location', '$routeParams', 'NotifyService', 'ReservationService', 'RoomService', 'UserService', 'ReservationStatusTypesService', ctrl ]);

	function ctrl($scope, $location, $routeParams, NotifyService, ReservationService, RoomService, UserService, ReservationStatusTypesService) {
		$('#sandbox-container input').datepicker({
			weekStart: 1,
			format: "yyyy-mm-dd",
		    todayBtn: "linked",
		    language: "en",
		    calendarWeeks: true,
		    autoclose: true,
		    todayHighlight: true
		});
		$scope.reservationStatusTypes = ReservationStatusTypesService.query();
		$scope.users = UserService.query();
		$scope.availableRooms = RoomService.query();		
		$scope.reservation = ReservationService.get({id: $routeParams.id});	
		$scope.save = function() {
			$scope.reservation.$save(function() {
				$location.path('/reservations');
				NotifyService.showSavedItem();
			});
		};
	}
})();
