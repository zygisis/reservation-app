var services = angular.module('reservationApp.services', [ 'ngResource' ]);

services.factory('UserService', function($resource) {
	return $resource(reservationAppConfig.restUrl+'user/:action', {}, {
		authenticate : {
			method : 'POST',
			params : {
				'action' : 'authenticate'
			},
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		},
		register : {
			method : 'POST',
			params : {
				'action' : 'register'
			},
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		},
		id : {
			method : 'GET',
			params : {
				'action' : '@id'
			}
		},
		documentIdTypes : {
			method : 'GET',
			params : {
				'action' : 'documentIdTypes'
			},
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		},			
	});
});

services.factory('NewsService', function($resource) {
	return $resource(reservationAppConfig.restUrl+'news/:id', {
		id : '@id'	
	});
});

services.factory('CreateNewsService', function($resource) {
	return $resource(reservationAppConfig.restUrl+'news/create', {});
});

services.factory('ReservationService', function($resource) {
	return $resource(reservationAppConfig.restUrl+'reservation/:id', {
		id : '@id'
	});
});

services.factory('RoomService', function($resource) {
	return $resource(reservationAppConfig.restUrl+'room/:id', {
		id : '@id'
	});
});

services.factory('ReservationStatusTypesService', function($resource) {
	return $resource(reservationAppConfig.restUrl+'reservation/reservationStatusTypes', {});
});

services.factory('VersionInfoService', function($resource) {
	return $resource(reservationAppConfig.restUrl+'app_info/version', {});
});

services.factory('UserTokenService', function($resource) {
	return $resource(reservationAppConfig.restUrl+'user/user_detail', {});
});