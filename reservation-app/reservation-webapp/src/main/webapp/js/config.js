var reservationAppConfig = {
	/* When set to false a query parameter is used to pass on the auth token.
	 * This might be desirable if headers don't work correctly in some
	 * environments and is still secure when using https. */
	useAuthTokenHeader: true,

//	restUrl: "https://reservation-rest.herokuapp.com/"
	restUrl: "http://localhost:8080/reservation-rest/"		
};