package com.shawk.model.enums;

import com.fasterxml.jackson.annotation.JsonFormat;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum RoleType {
	
	ADMIN(true, true, true),
	CONTROLLER(true, true, false),
	JUNIOR(true, true, false),
	GUEST;
	
	private boolean reservation;
	private boolean clientData;
	private boolean userControl;
	
	private RoleType(){
		
	}
	private RoleType(boolean reservation, boolean clientData, boolean userControl) {
		this.reservation = reservation;
		this.clientData = clientData;
		this.userControl = userControl;
	}
	public boolean isReservation() {
		return reservation;
	}
	public void setReservation(boolean reservation) {
		this.reservation = reservation;
	}
	public boolean isClientData() {
		return clientData;
	}
	public void setClientData(boolean clientData) {
		this.clientData = clientData;
	}
	public boolean isUserControl() {
		return userControl;
	}
	public void setUserControl(boolean userControl) {
		this.userControl = userControl;
	}
}