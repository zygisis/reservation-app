package com.shawk.model.enums;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonValue;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum RoomType {
	SINGLE("Single"), 
	DOUBLE("Double"),
	PRESIDENTIAL("Presidental"),
	PACIFIC("Pacific"),
	DELUXE("Deluxe"),
	EXECUTIVE("Executive"),
	SUPERIOR("Superior");
	
	private RoomType(String displayValue) {
		this.displayValue = displayValue;
	}
	
	private String displayValue;

	@JsonValue
	public String getDisplayValue() {
		return displayValue;
	}

	public void setDisplayValue(String displayValue) {
		this.displayValue = displayValue;
	}
	
}