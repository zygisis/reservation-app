package com.shawk.model.enums;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonValue;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum FloorType {
	FIRST("First"), 
	SECOND("Second"),
	THIRD("Third"),
	FOURTH("Fourth"),
	FIFTH("Fifth"),
	SIXTH("Sixth"),
	SEVENTH("Seventh"),
	EIGHTH("Eighth");
	
	private FloorType(String displayValue) {
		this.displayValue = displayValue;
	}
	
	private String displayValue;

	@JsonValue
	public String getDisplayValue() {
		return displayValue;
	}

	public void setDisplayValue(String displayValue) {
		this.displayValue = displayValue;
	}
	
}