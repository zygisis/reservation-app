package com.shawk.model;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.joda.time.DateTime;

import com.shawk.model.enums.FloorType;
import com.shawk.model.enums.ReservationStatusType;
import com.shawk.model.enums.RoomType;
import com.shawk.model.utils.Consts;
import com.shawk.model.utils.LConst;

@Entity
@Table(name = "vw_reservation")
public class ViewReservation implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "reservation_id")
	private Long id;

	@Column(name = "room_number")
	private int roomNumber;

	@Column(name = "room_floor", length = LConst.L50)
	@Enumerated(EnumType.STRING)
	private FloorType roomFloor;

	@Column(name = "room_has_internet")
	private boolean internet;

	@Column(name = "room_has_tv")
	private boolean television;

	@Column(name = "room_has_phone")
	private boolean phone;

	@Column(name = "reservation_start")
	@Temporal(TemporalType.DATE)
	private Date reservationStart;

	@Column(name = "reservation_end")
	@Temporal(TemporalType.DATE)
	private Date reservationEnd;

	@Column(name = "reservation_status", length = LConst.L50)
	@Enumerated(EnumType.STRING)
	private ReservationStatusType reservationStatus;

	@Column(name = "room_type", length = LConst.L50)
	@Enumerated(EnumType.STRING)
	private RoomType roomType;

	@Column(name = "reserved_by")
	private String reservedBy;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "user_id", referencedColumnName = "id")
	private User user;

	protected ViewReservation() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public RoomType getRoomType() {
		return roomType;
	}

	public void setRoomType(RoomType roomType) {
		this.roomType = roomType;
	}

	public int getRoomNumber() {
		return roomNumber;
	}

	public void setRoomNumber(int roomNumber) {
		this.roomNumber = roomNumber;
	}

	public FloorType getRoomFloor() {
		return roomFloor;
	}

	public void setRoomFloor(FloorType roomFloor) {
		this.roomFloor = roomFloor;
	}

	public boolean isInternet() {
		return internet;
	}

	public void setInternet(boolean internet) {
		this.internet = internet;
	}

	public boolean isTelevision() {
		return television;
	}

	public void setTelevision(boolean television) {
		this.television = television;
	}

	public boolean isPhone() {
		return phone;
	}

	public void setPhone(boolean phone) {
		this.phone = phone;
	}

	public Date getReservationStart() {
		return reservationStart;
	}

	public void setReservationStart(Date reservationStart) {
		this.reservationStart = reservationStart;
	}

	public Date getReservationEnd() {
		return reservationEnd;
	}

	public void setReservationEnd(Date reservationEnd) {
		this.reservationEnd = reservationEnd;
	}

	public ReservationStatusType getReservationStatus() {
		return reservationStatus;
	}

	public void setReservationStatus(ReservationStatusType reservationStatus) {
		this.reservationStatus = reservationStatus;
	}

	public String getReservedBy() {
		return reservedBy;
	}

	public void setReservedBy(String reservedBy) {
		this.reservedBy = reservedBy;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public boolean isCanGuestCancel() {
		if (getReservationStart() == null) {
			return true;
		}
		Calendar startDate = Calendar.getInstance();
		startDate.setTime(getReservationStart());
		startDate.set(Calendar.HOUR_OF_DAY, Consts.RESERVATION_START);

		DateTime startTime = new DateTime(startDate);
		startTime = startTime.minusDays(Consts.DAYS);

		DateTime now = new DateTime();

		return getUser().isGuest() && now.toDate().before(startTime.toDate());
	}

}