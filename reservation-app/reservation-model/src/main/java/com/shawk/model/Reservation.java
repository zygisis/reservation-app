package com.shawk.model;

import java.util.Calendar;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.joda.time.DateTime;

import com.shawk.model.enums.ReservationStatusType;
import com.shawk.model.interfaces.IEntity;
import com.shawk.model.utils.Consts;
import com.shawk.model.utils.LConst;

@Entity
@Table(name = "reservation", schema = "public")
public class Reservation implements IEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "room_id", referencedColumnName = "id")
	private Room room;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "user_id", referencedColumnName = "id")
	private User user;

	@Column(name = "reservation_status", nullable = false, length = LConst.L50)
	@Enumerated(EnumType.STRING)
	private ReservationStatusType reservationStatus = ReservationStatusType.PENDING_APPROVAL;

	@Column(name = "paid")
	private boolean paid;

	@Column(name = "start_date")
	@Temporal(TemporalType.DATE)
	private Date start;

	@Column(name = "end_date")
	@Temporal(TemporalType.DATE)
	private Date end;
	
	@Transient
	private String startDisplayValue;
	
	@Transient
	private String endDisplayValue;
	
	@Transient
	private boolean canGuestCancel;

	protected Reservation() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Room getRoom() {
		return room;
	}

	public void setRoom(Room room) {
		this.room = room;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public ReservationStatusType getReservationStatus() {
		return reservationStatus;
	}

	public void setReservationStatus(ReservationStatusType reservationStatus) {
		this.reservationStatus = reservationStatus;
	}

	public boolean isPaid() {
		return paid;
	}

	public void setPaid(boolean paid) {
		this.paid = paid;
	}

	public Date getStart() {
		return start;
	}

	public void setStart(Date start) {
		this.start = start;
	}

	public Date getEnd() {
		return end;
	}

	public void setEnd(Date end) {
		this.end = end;
	}

	public String getStartDisplayValue() {
		return startDisplayValue;
	}

	public void setStartDisplayValue(String startDisplayValue) {
		this.startDisplayValue = startDisplayValue;
	}

	public String getEndDisplayValue() {
		return endDisplayValue;
	}

	public void setEndDisplayValue(String endDisplayValue) {
		this.endDisplayValue = endDisplayValue;
	}

	public boolean isCanGuestCancel() {
		if (getStart() == null) {
			return true;
		}		
		
		Calendar startDate = Calendar.getInstance();
		startDate.setTime(getStart());
		startDate.set(Calendar.HOUR_OF_DAY, Consts.RESERVATION_START);
		
		DateTime startTime = new DateTime(startDate);
		startTime = startTime.minusDays(Consts.DAYS);
		
		DateTime now = new DateTime();
				
		return getUser().isGuest() && now.toDate().before(startTime.toDate());
	}

}