package com.shawk.model.utils;

public final class Consts {
	public static final int DAYS = 2;
	public static final int RESERVATION_START = 12;

	public static final String VERSION_PROPS = "version.properties";
	public static final String APP_VERSION = "appVersion";
	public static final String BUILD_TIME = "buildTime";

	public static final String SPACE = " ";
	public static final String COMMA_SEPERATOR = "," + SPACE;
	public static final String DELIM_DASH = "-";
	public static final String BRACKET_LEFT = "(";
	public static final String BRACKET_RIGHT = ")";
}