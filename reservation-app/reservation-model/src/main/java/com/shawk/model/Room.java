package com.shawk.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.shawk.model.enums.FloorType;
import com.shawk.model.enums.RoomType;
import com.shawk.model.interfaces.IEntity;
import com.shawk.model.utils.LConst;

@Entity
@Table(name = "room", schema = "public")
public class Room implements IEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "number")
	private int number;

	@Column(name = "floor_type", length = LConst.L50)
	@Enumerated(EnumType.STRING)
	private FloorType floor;

	@Column(name = "capacity")
	private int capacity;

	@Column(name = "has_internet")
	private boolean internet;

	@Column(name = "has_tv")
	private boolean television;

	@Column(name = "has_phone")
	private boolean phone;

	@Column(name = "room_type", length = LConst.L50)
	@Enumerated(EnumType.STRING)
	private RoomType roomType;

	protected Room() {

	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getCapacity() {
		return capacity;
	}

	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}

	public RoomType getRoomType() {
		return roomType;
	}

	public void setRoomType(RoomType roomType) {
		this.roomType = roomType;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public boolean isInternet() {
		return internet;
	}

	public void setInternet(boolean internet) {
		this.internet = internet;
	}

	public boolean isTelevision() {
		return television;
	}

	public void setTelevision(boolean television) {
		this.television = television;
	}

	public boolean isPhone() {
		return phone;
	}

	public void setPhone(boolean phone) {
		this.phone = phone;
	}

	public FloorType getFloor() {
		return floor;
	}

	public void setFloor(FloorType floor) {
		this.floor = floor;
	}

}