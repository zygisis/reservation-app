package com.shawk.model.enums;

import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonValue;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum ReservationStatusType {
	VACANT("Vacant"), 
	STAYOVER("Stay over"),
	CHECKOUT("Checkout"),
	MAINTENANCE("Maintenance"),
	PENDING_APPROVAL("Pending approval"),
	ERRORNOUS("Errornous"),
	CANCELED("Canceled"),
	RESERVED("Reserved"),
	PENDING_CANCELATION("Pending cancelation"),
	RESERVATION_CANCELED("Reservation canceled");
	
	public static List<ReservationStatusType> OCCUPIED_STATUSES = Arrays.asList(RESERVED, VACANT, STAYOVER);
	public static List<ReservationStatusType> VALID_FOR_GUEST = Arrays.asList(PENDING_APPROVAL, PENDING_CANCELATION);

	private ReservationStatusType(String displayValue) {
		this.displayValue = displayValue;
	}
	
	private String displayValue;

	@JsonValue
	public String getDisplayValue() {
		return displayValue;
	}
	public void setDisplayValue(String displayValue) {
		this.displayValue = displayValue;
	}	
	
}