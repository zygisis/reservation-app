package com.shawk.bl.security;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;

public interface IAuthenticationFacade {

	Authentication getAuthentication();
	
	void authenticate(String username, String password);
	
	void authenticate(UsernamePasswordAuthenticationToken token);
}
