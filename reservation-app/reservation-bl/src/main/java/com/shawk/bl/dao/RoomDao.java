package com.shawk.bl.dao;

import java.util.List;

import com.shawk.model.Room;

public interface RoomDao extends Dao<Room, Long> {

	List<Room> findAvailable();

}