/**
 * 
 */
package com.shawk.bl.helper;

import java.util.Map;

import org.springframework.web.multipart.MultipartFile;

import com.shawk.model.User;

/**
 * @author zygis
 *
 */
public interface FileHelper {

	void processFiles(Map<String, MultipartFile> uploadedfiles, User currentUser);
	
}
