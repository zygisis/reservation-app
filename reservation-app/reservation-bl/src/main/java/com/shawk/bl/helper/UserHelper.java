package com.shawk.bl.helper;

import java.security.Principal;

import org.springframework.security.core.userdetails.UserDetails;

import com.shawk.bl.security.UserTransfer;
import com.shawk.model.User;

public interface UserHelper {

	User getCurrentUser();

	boolean isCurrentUserAdmin();

	boolean isCurrentUserGuest();

	UserDetails authenticate(String username, String password);

	UserTransfer getUserTransfer(Principal principal);
}
