package com.shawk.bl.dao;

import java.util.List;

import org.springframework.security.core.userdetails.UserDetailsService;

import com.shawk.model.User;

public interface UserDao extends Dao<User, Long>, UserDetailsService {

	User findByUserName(String userName);

	List<User> findAllWithReadblePasses();
}