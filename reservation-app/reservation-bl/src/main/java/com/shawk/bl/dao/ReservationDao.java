package com.shawk.bl.dao;

import java.util.List;

import com.shawk.model.Reservation;
import com.shawk.model.User;
import com.shawk.model.ViewReservation;

public interface ReservationDao extends Dao<Reservation, Long> {

	List<ViewReservation> findReservations(User user);

}