package com.shawk.bl.dao;

import com.shawk.model.NewsEntry;

public interface NewsEntryDao extends Dao<NewsEntry, Long> {

}