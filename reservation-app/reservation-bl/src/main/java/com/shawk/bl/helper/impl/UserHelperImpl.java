package com.shawk.bl.helper.impl;

import java.security.Principal;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import com.shawk.bl.dao.UserDao;
import com.shawk.bl.helper.UserHelper;
import com.shawk.bl.security.IAuthenticationFacade;
import com.shawk.bl.security.UserTransfer;
import com.shawk.model.User;

@Component
public class UserHelperImpl implements UserHelper {

	@Autowired
	private UserDao userDao;
	@Autowired
	private IAuthenticationFacade authenticationFacade;

	@Override
	public User getCurrentUser() {
		try {
			if (authenticationFacade.getAuthentication().isAuthenticated()) {
				return (User) authenticationFacade.getAuthentication().getPrincipal();
			}			
		} catch (Exception e) {
			System.out.println("Current user error -> " + e.getMessage());
		}
		return userDao.find(1L);
	}

	@Override
	public boolean isCurrentUserAdmin() {
		return getCurrentUser() != null && getCurrentUser().isAdmin();
	}

	@Override
	public boolean isCurrentUserGuest() {
		return getCurrentUser() != null && getCurrentUser().isGuest();
	}

	private Map<String, Boolean> createRoleMap(User currentUser) {
		Map<String, Boolean> roles = new HashMap<String, Boolean>();
		for (GrantedAuthority authority : currentUser.getAuthorities()) {
			roles.put(authority.getAuthority(), Boolean.TRUE);
		}

		return roles;
	}

	@Override
	public UserDetails authenticate(String username, String password) {
		authenticationFacade.authenticate(username, password);
		// Reload user as password of authentication principal will be null
		// after
		// authorization and password is needed for token generation
		return userDao.loadUserByUsername(username);
	}

	@Override
	public UserTransfer getUserTransfer(Principal principal) {
		User currentUser = null;
		if (principal != null) {
			if (principal instanceof UserDetails) {
				currentUser = (User) principal;
			} else if (principal instanceof UsernamePasswordAuthenticationToken) {
				UsernamePasswordAuthenticationToken token = (UsernamePasswordAuthenticationToken) principal;
				currentUser = (User) token.getPrincipal();
			}
			return new UserTransfer(currentUser.getSummary(), createRoleMap(currentUser));
		}
		return null;
	}

}
