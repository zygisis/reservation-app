package com.shawk.bl.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;

import com.shawk.bl.dao.ReservationDao;
import com.shawk.bl.utils.PersistenceUtils;
import com.shawk.model.Reservation;
import com.shawk.model.Reservation_;
import com.shawk.model.User;
import com.shawk.model.User_;
import com.shawk.model.ViewReservation;
import com.shawk.model.ViewReservation_;

@Repository
public class ReservationDaoImpl extends DaoImpl<Reservation, Long> implements ReservationDao {

	public ReservationDaoImpl() {
		super(Reservation.class);
	}

	@Override
	public List<Reservation> findAll() {
		final CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
		final CriteriaQuery<Reservation> criteriaQuery = builder.createQuery(Reservation.class);

		Root<Reservation> root = criteriaQuery.from(Reservation.class);
		criteriaQuery.orderBy(builder.desc(root.get(Reservation_.START)));

		TypedQuery<Reservation> typedQuery = getEntityManager().createQuery(criteriaQuery);
		return typedQuery.getResultList();
	}

	
	/**
	 * For guest role users selecting reservations only for them
	 * */
	@Override
	public List<ViewReservation> findReservations(User user) {
		final CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
		final CriteriaQuery<ViewReservation> criteriaQuery = builder.createQuery(ViewReservation.class);

		Root<ViewReservation> root = criteriaQuery.from(ViewReservation.class);

//		ParameterExpression<Date> nowParam = builder.parameter(Date.class);
		ParameterExpression<Long> userIdParam = builder.parameter(Long.class);
		
//		Path<ReservationStatusType> pathReservationStatusType = root.get(ViewReservation_.RESERVATION_STATUS);
		Path<Date> pathReservationStart = root.get(ViewReservation_.RESERVATION_START);
//		Path<Date> pathReservationEnd = root.get(ViewReservation_.RESERVATION_END);
		Path<Long> pathReservationUser = root.get(ViewReservation_.USER).get(User_.ID);

		List<Predicate> predicates = new ArrayList<Predicate>();
		if (user != null) {
			predicates.add(builder.equal(pathReservationUser, userIdParam));	
		}
		
		//predicates.add(builder.not(pathReservationStatusType.in(ReservationStatusType.OCCUPIED_STATUSES)));
		//predicates.add(builder.lessThanOrEqualTo(pathReservationStart, nowParam));
		//predicates.add(builder.or(builder.isNull(pathReservationEnd), builder.greaterThanOrEqualTo(pathReservationEnd, nowParam)));

		criteriaQuery.where(PersistenceUtils.toArray(predicates));
		criteriaQuery.orderBy(builder.desc(pathReservationStart));

		TypedQuery<ViewReservation> query = getEntityManager().createQuery(criteriaQuery);
		if (user != null) {
			query.setParameter(userIdParam, user.getId());
		}
		//query.setParameter(nowParam, Calendar.getInstance().getTime());
		
		return query.getResultList();
	}

}
