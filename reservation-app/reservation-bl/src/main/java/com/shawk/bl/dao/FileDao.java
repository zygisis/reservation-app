package com.shawk.bl.dao;

import java.util.List;
import java.util.Map;

import org.springframework.web.multipart.MultipartFile;

import com.shawk.model.File;
import com.shawk.model.User;

public interface FileDao extends Dao<File, Long> {

	List<File> findAll(User user);

	void removeAll(User user);
	
	void saveMultipartFiles(Map<String, MultipartFile> uploadedfiles, User currentUser);

}