package com.shawk.bl.dao.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;

import org.springframework.stereotype.Repository;

import com.shawk.bl.dao.RoomDao;
import com.shawk.bl.utils.PersistenceUtils;
import com.shawk.model.Reservation;
import com.shawk.model.Reservation_;
import com.shawk.model.Room;
import com.shawk.model.enums.ReservationStatusType;

@Repository
public class RoomDaoImpl extends DaoImpl<Room, Long> implements RoomDao {

	public RoomDaoImpl() {
		super(Room.class);
	}

	@Override
	public List<Room> findAll() {
		final CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
		final CriteriaQuery<Room> criteriaQuery = builder.createQuery(Room.class);

		Root<Room> root = criteriaQuery.from(Room.class);
		criteriaQuery.select(root);

		TypedQuery<Room> typedQuery = getEntityManager().createQuery(criteriaQuery);
		return typedQuery.getResultList();
	}

	@Override
	public List<Room> findAvailable() {
		final CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
		final CriteriaQuery<Room> criteriaQuery = builder.createQuery(Room.class);

		Root<Room> root = criteriaQuery.from(Room.class);
		
		ParameterExpression<Date> nowParam = builder.parameter(Date.class);
		
		Subquery<Reservation> subquery = criteriaQuery.subquery(Reservation.class);
		Root<Reservation> rootReservation = subquery.from(Reservation.class);
		
		Path<Room> subqueryRoomPath = rootReservation.get(Reservation_.ROOM);
		Path<ReservationStatusType> subqueryReservationStatusTypePath = rootReservation.get(Reservation_.RESERVATION_STATUS);
		Path<Date> subqueryReservationStartPath = rootReservation.get(Reservation_.START);
		Path<Date> subqueryReservationEndPath = rootReservation.get(Reservation_.END);
		
		List<Predicate> subqueryPredicates = new ArrayList<Predicate>();
		subqueryPredicates.add(builder.equal(subqueryRoomPath, root));
		subqueryPredicates.add(builder.not(subqueryReservationStatusTypePath.in(ReservationStatusType.OCCUPIED_STATUSES)));
		subqueryPredicates.add(builder.lessThanOrEqualTo(subqueryReservationStartPath, nowParam));
		subqueryPredicates.add(builder.or(builder.isNull(subqueryReservationEndPath), builder.greaterThanOrEqualTo(subqueryReservationEndPath, nowParam)));		
		
		subquery.select(rootReservation); // field to map with main-query
		subquery.where(PersistenceUtils.toArray(subqueryPredicates));

		List<Predicate> predicates = new ArrayList<Predicate>();
		predicates.add(builder.not(builder.exists(subquery)));
		
		criteriaQuery.select(root);
		criteriaQuery.where(PersistenceUtils.toArray(predicates));

		TypedQuery<Room> query = getEntityManager().createQuery(criteriaQuery);
		query.setParameter(nowParam, Calendar.getInstance().getTime());
		
		return query.getResultList();
	}

}
