package com.shawk.rest.resources;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.shawk.bl.dao.ReservationDao;
import com.shawk.bl.helper.UserHelper;
import com.shawk.model.Reservation;
import com.shawk.model.User;
import com.shawk.model.ViewReservation;
import com.shawk.model.enums.ReservationStatusType;
import com.shawk.rest.utils.RestPaths;

@CrossOrigin
@RestController
@RequestMapping("/reservation")
public class ReservationResource {

	@Autowired
	private ReservationDao reservationDao;

	@Autowired
	private UserHelper userHelper;

	@GetMapping(RestPaths.ID_PATH)
	public Reservation read(@PathVariable(RestPaths.ID) Long id) {
		return reservationDao.find(id);
	}

	@PostMapping
	public Reservation create(@RequestParam("reservation") Reservation reservation) {
		if (reservation.getUser() == null && userHelper.isCurrentUserGuest()) {
			reservation.setUser(userHelper.getCurrentUser());
		}
		return reservationDao.save(reservation);
	}

	@PutMapping(RestPaths.ID_PATH)
	public Reservation update(@PathVariable(RestPaths.ID) Long id, Reservation reservation) {
		//FIXME: After few editings front-end loses user, so before saving getting the right one
		User user = reservationDao.find(id).getUser();
		reservation.setUser(user);
		return reservationDao.save(reservation);
	}

	@DeleteMapping(RestPaths.ID_PATH)
	public void delete(@PathVariable(RestPaths.ID) Long id) {
		reservationDao.delete(id);
	}
	
	@GetMapping
	public List<ViewReservation> list() {
		return reservationDao.findReservations(userHelper.isCurrentUserGuest() ? userHelper.getCurrentUser() : null);
	}	

	@GetMapping("/reservationStatusTypes")
	public List<ReservationStatusType> reservationStatusTypes() {
		if (userHelper.isCurrentUserGuest()) {
			return ReservationStatusType.VALID_FOR_GUEST;
		}
		return Arrays.asList(ReservationStatusType.values());
	}
	
}