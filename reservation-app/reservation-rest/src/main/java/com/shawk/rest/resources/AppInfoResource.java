package com.shawk.rest.resources;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.shawk.model.info.AppInfo;
import com.shawk.model.utils.Consts;

@CrossOrigin
@RestController
@PropertySource({ "classpath:" + Consts.VERSION_PROPS })
@RequestMapping("/app_info")
public class AppInfoResource {

	@Autowired
	private Environment env;

	@GetMapping("/version")
	public AppInfo getAppInfo() {
		return new AppInfo(env.getProperty(Consts.APP_VERSION), env.getProperty(Consts.BUILD_TIME));
	}

}