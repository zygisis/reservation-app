package com.shawk.rest.resources;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.shawk.bl.dao.RoomDao;
import com.shawk.model.Room;
import com.shawk.rest.utils.RestPaths;

@CrossOrigin
@RestController
@RequestMapping("/room")
public class RoomResource {

	@Autowired
	private RoomDao roomDao;

	@GetMapping(RestPaths.ID_PATH)
	public Room read(@PathVariable(RestPaths.ID) Long id) {
		return roomDao.find(id);
	}

	@PostMapping
	public Room create(@RequestParam("room") Room room) {
		return roomDao.save(room);
	}

	@PutMapping(RestPaths.ID_PATH)
	public Room update(@PathVariable(RestPaths.ID) Long id, Room room) {
		return roomDao.save(room);
	}

	@DeleteMapping(RestPaths.ID_PATH)
	public void delete(@PathVariable(RestPaths.ID) Long id) {
		roomDao.delete(id);
	}

	@GetMapping
	public List<Room> list() {
		return roomDao.findAvailable();
	}

}